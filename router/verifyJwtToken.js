const jwt = require('jsonwebtoken');
const url = require('url');
const config = require('../config/config.js');
const db = require('../config/db.config.js');
const Role = db.role;
const User = db.user;
const Offerwall = db.offerwall;
const Slider = db.slider;
const Games = db.games;
const Gamelaps = db.gamelaps;
const Paymentmethods = db.paymentmethods;
const References = db.references;
const Userjokers = db.userjokers;
const Transactions = db.transactions;
const Scratchwin = db.scratchwin;
const Support = db.support;
const Users = db.users;
const Scratches = db.scratches;
const Boxes = db.boxes;
const Usercoins = db.usercoins;
const Withdraws = db.withdraws;
const Bonus = db.bonus;
const Bonuses = db.bonuses;
const Notifications = db.notifications;
const Faq = db.faq;
const Offers = db.offers;
const Settings = db.settings;

verifyToken = (req, res, next) => {
	//let token = req.headers['x-access-token'];

	let token = req.headers.authorization.split(" ")[1];
  
	if (!token){
		return res.status(403).send({ 
			auth: false, message: 'No token provided.' 
		});
	}

	jwt.verify(token, config.secret, (err, decoded) => {
		if (err){
			return res.status(500).send({ 
					auth: false, 
					message: 'Fail to Authentication. Error -> ' + err 
				});
		}
		
		req.userId = decoded.id;

		if(req.method == "GET") {
			var path = url.parse(req.url).pathname.split("/")[2];
			var path = path.charAt(0).toUpperCase() + path.slice(1);

			var filters = {};
			if(path != 'Postback' && path != 'Scratchrandom') {
				var modelName = eval(path + ".rawAttributes");
				for( let key in modelName){
				    if(req.query[key]) {
				    	filters[key] = req.query[key];
				    }
				}
				var fields = [];
				if(req.query['fields']) {
				    for(let field in req.query['fields'].split(',')) {
				    	fields.push(req.query['fields'].split(',')[field]);
				    }
				    req.fields = fields;
				}
			}
			req.filters = filters;
		}
		
		next();
	});
}

isAdmin = (req, res, next) => {
	let token = req.headers['x-access-token'];
	
	User.findById(req.userId)
		.then(user => {
			user.getRoles().then(roles => {
				for(let i=0; i<roles.length; i++){
					if(roles[i].name.toUpperCase() === "ADMIN"){
						next();
						return;
					}
				}
				
				res.status(403).send("Require Admin Role!");
				return;
			})
		})
}

isPmOrAdmin = (req, res, next) => {
	let token = req.headers['x-access-token'];
	
	User.findById(req.userId)
		.then(user => {
			user.getRoles().then(roles => {
				for(let i=0; i<roles.length; i++){					
					if(roles[i].name.toUpperCase() === "PM"){
						next();
						return;
					}
					
					if(roles[i].name.toUpperCase() === "ADMIN"){
						next();
						return;
					}
				}
				
				res.status(403).send("Require PM or Admin Roles!");
			})
		})
}

const authJwt = {};
authJwt.verifyToken = verifyToken;
authJwt.isAdmin = isAdmin;
authJwt.isPmOrAdmin = isPmOrAdmin;

module.exports = authJwt;