const verifySignUp = require('./verifySignUp');
const authJwt = require('./verifyJwtToken');

module.exports = function(app) {

    const controller = require('../controller/controller.js');
    const userjokers = require('../controller/UserJokersController.js');
    const references = require('../controller/ReferencesController.js');
    const offerwall = require('../controller/OfferwallController.js');
    const games = require('../controller/GamesController.js');
    const gamelaps = require('../controller/GameLapsController.js');
    const paymentmethods = require('../controller/PaymentMethodsController.js');
    const transactions = require('../controller/TransactionsController.js');
    const scratchwin = require('../controller/ScratchWinController.js');
    const support = require('../controller/SupportController.js');
    const users = require('../controller/UsersController.js');
    const stats = require('../controller/StatsController.js');
    const adgate = require('../controller/AdgateController.js');
    const admob = require('../controller/AdmobController.js');
    const slider = require('../controller/SliderController.js');
    const offerwalllist = require('../controller/OfferwallListController.js');
    const coincap = require('../controller/CoincapController.js');
    const coins = require('../controller/CoinsController.js');
    const usercoins = require('../controller/UserCoinsController.js');
    const scratchrandom = require('../controller/ScratchRandomController.js');
    const scratches = require('../controller/ScratchesController.js');
    const boxes = require('../controller/BoxesController.js');
    const withdraws = require('../controller/WithdrawsController.js');
    const bonus = require('../controller/BonusController.js');
    const bonuses = require('../controller/BonusesController.js');
    const faq = require('../controller/FaqController.js');
	const settings = require('../controller/SettingsController.js');

	app.post('/api/auth/register', [verifySignUp.checkDuplicateUserNameOrEmail], controller.signup);
	
	app.post('/api/auth/signin', controller.signin);
	
	app.get('/api/test/user', [authJwt.verifyToken], controller.userContent);

	app.post('/api/auth/verify', [authJwt.verifyToken], controller.verify);
	app.post('/api/auth/verifycomplete', [authJwt.verifyToken], controller.verifyComplete);

	app.post('/api/user/update', [authJwt.verifyToken], users.post);

	app.post('/api/user/updateos', [authJwt.verifyToken], users.updateos);
	app.post('/api/user/updatedevice', [authJwt.verifyToken], users.updatedevice);
	app.post('/api/user/updatebinanceid', [authJwt.verifyToken], users.updatebinanceid);
	
	app.get('/api/test/pm', [authJwt.verifyToken, authJwt.isPmOrAdmin], controller.managementBoard);
	
	app.get('/api/test/admin', [authJwt.verifyToken, authJwt.isAdmin], controller.adminBoard);

	app.get('/api/userjokers', [authJwt.verifyToken], userjokers.get);
	app.post('/api/userjokers', [authJwt.verifyToken], userjokers.update);

	app.get('/api/references', [authJwt.verifyToken], references.get);
	app.get('/api/offerwall', [authJwt.verifyToken], offerwall.get);
	app.get('/api/games', [authJwt.verifyToken], games.get);
	app.post('/api/newgame', [authJwt.verifyToken], games.post);

	//Boxes
	app.get('/api/boxes', [authJwt.verifyToken], boxes.get);
	app.post('/api/boxes', [authJwt.verifyToken], boxes.post);

	//GameLap
	app.get('/api/gamelaps', [authJwt.verifyToken], gamelaps.get);
	app.post('/api/gamelap/card/update', [authJwt.verifyToken], gamelaps.updateCard);
	app.post('/api/gamelap/result/update', [authJwt.verifyToken], gamelaps.updateResult);
	app.post('/api/gamelap/lessthan', [authJwt.verifyToken], gamelaps.lessThan);
	app.post('/api/gamelap/greaterthan', [authJwt.verifyToken], gamelaps.greaterThan);
	app.post('/api/gamelap/getprize', [authJwt.verifyToken], gamelaps.getPrize);
	app.post('/api/gamelap/continuegame', [authJwt.verifyToken], gamelaps.continueGame);
	app.post('/api/gamelap/joker/use', [authJwt.verifyToken], gamelaps.useJoker);

	app.get('/api/paymentmethods', [authJwt.verifyToken], paymentmethods.get);
	app.get('/api/transactions', [authJwt.verifyToken], transactions.get);
	app.post('/api/transactions', [authJwt.verifyToken], transactions.post);
	app.get('/api/support', [authJwt.verifyToken], support.get);
	app.post('/api/support', [authJwt.verifyToken], support.post);
	app.get('/api/users', [authJwt.verifyToken], users.get);
	app.get('/api/stats', stats.get);
	app.get('/api/settings', settings.get);
	app.get('/api/slider', slider.get);

	app.get('/api/faq', faq.get);

	app.get('/api/scratchrandom', [authJwt.verifyToken], scratchrandom.get);
	app.get('/api/scratches', [authJwt.verifyToken], scratches.get);
	app.post('/api/scratches', [authJwt.verifyToken], scratches.post);

	app.get('/api/withdraws', [authJwt.verifyToken], withdraws.get);
	app.post('/api/withdraws', [authJwt.verifyToken], withdraws.post);

	app.get('/api/postback/adgate', adgate.get);
	app.get('/api/postback/admob', [authJwt.verifyToken], admob.get);
	app.post('/api/postback/admob', [authJwt.verifyToken], admob.post);

	app.get('/api/bonuses', [authJwt.verifyToken], bonuses.get);
	app.get('/api/bonus', [authJwt.verifyToken], bonus.get);
	app.post('/api/bonus', [authJwt.verifyToken], bonus.post);

	app.get('/api/offers', offerwalllist.get);
	app.get('/api/offerwalllist', offerwalllist.adgate);
	
	app.get('/api/coincap', coincap.get);
	app.get('/api/coins', coins.get);
	app.get('/api/usercoins', [authJwt.verifyToken], usercoins.get);

}