const env = require('./env.js');
 
const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
  operatorsAliases: false,
 
  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle
  }
});
 
const db = {};
 
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.port = 3001;
 
db.user = require('../model/user.model.js')(sequelize, Sequelize);
db.role = require('../model/role.model.js')(sequelize, Sequelize);
db.userjokers = require('../model/userjokers.model.js')(sequelize, Sequelize);
db.references = require('../model/references.model.js')(sequelize, Sequelize);
db.offerwall = require('../model/offerwall.model.js')(sequelize, Sequelize);
db.games = require('../model/games.model.js')(sequelize, Sequelize);
db.gamelaps = require('../model/gamelaps.model.js')(sequelize, Sequelize);
db.paymentmethods = require('../model/paymentmethods.model.js')(sequelize, Sequelize);
db.transactions = require('../model/transactions.model.js')(sequelize, Sequelize);
db.scratchwin = require('../model/scratchwin.model.js')(sequelize, Sequelize);
db.support = require('../model/support.model.js')(sequelize, Sequelize);
db.users = require('../model/users.model.js')(sequelize, Sequelize);
db.stats = require('../model/stats.model.js')(sequelize, Sequelize);
db.clickrewarded = require('../model/clickrewarded.model.js')(sequelize, Sequelize);
db.slider = require('../model/slider.model.js')(sequelize, Sequelize);
db.coins = require('../model/coins.model.js')(sequelize, Sequelize);
db.usercoins = require('../model/usercoins.model.js')(sequelize, Sequelize);
db.scratches = require('../model/scratches.model.js')(sequelize, Sequelize);
db.boxes = require('../model/boxes.model.js')(sequelize, Sequelize);
db.withdraws = require('../model/withdraws.model.js')(sequelize, Sequelize);
db.bonus = require('../model/bonus.model.js')(sequelize, Sequelize);
db.bonuses = require('../model/bonuses.model.js')(sequelize, Sequelize);
db.notifications = require('../model/notifications.model.js')(sequelize, Sequelize);
db.faq = require('../model/faq.model.js')(sequelize, Sequelize);
db.offers = require('../model/offers.model.js')(sequelize, Sequelize);
db.settings = require('../model/settings.model.js')(sequelize, Sequelize);

db.role.belongsToMany(db.user, { through: 'user_roles', foreignKey: 'roleId', otherKey: 'userId'});
db.user.belongsToMany(db.role, { through: 'user_roles', foreignKey: 'userId', otherKey: 'roleId'});

db.coins.hasMany(db.usercoins, {foreignKey : 'coin_id', as : 'usercoins'});
db.usercoins.belongsTo(db.coins, {foreignKey : 'id'});

db.coins.hasMany(db.withdraws, {foreignKey : 'coin_id', as : 'withdraws'});
db.withdraws.belongsTo(db.coins, {foreignKey : 'coin_id', as: 'coins'});

db.coins.hasMany(db.boxes, {foreignKey : 'coin_id', as : 'boxes'});
db.boxes.belongsTo(db.coins, {foreignKey : 'coin_id', as: 'coins'});

//db.coins.hasMany(db.withdraws, {foreignKey : 'coin_id', as : 'withdraws'});
//db.coins.hasMany(db.withdraws, {foreignKey : 'coin_id', as : 'withdraws'});
//db.withdraws.belongsTo(db.coins, { foreignKey: 'coin_id' });
//db.coins.hasMany(db.withdraws, { foreignKey: 'coin_id' });

//Address.belongsTo(Customers, {foreignKey: 'fk_customerid', targetKey: 'uuid'});
//Customers.hasOne(Address, {foreignKey: 'fk_customerid', targetKey: 'uuid'});

module.exports = db;