const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Transactions = db.transactions;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	req.filters['user_id'] = req.userId;

	Transactions.findAll({
		where: req.filters,
		attributes: ['id', 'user_id', 'amount', 'type', 'payment_status', 'status', 'created_date', 'confirm_date']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.post = (req, res) => {

	Transactions.create({
		user_id: req.userId,
		amount: req.body.amount,
		type: req.body.type,
		status: 0
	}).then(game => {
		res.status(200).json({
			"status": 200
		});
	}).catch(err => {
		res.status(500).send("Fail! Error -> " + err);
	})
	
}