const db = require('../config/db.config.js');
const Boxes = db.boxes;
const Users = db.users;
const Coins = db.coins;
const Usercoins = db.usercoins;
const Stats = db.stats;

var moment = require('moment');

exports.get = (req, res) => {

    var offset = 0;
    if(req.query.start) {
        offset = req.query.start;
    }

	Boxes.findAll({
        offset: parseInt(offset),
        limit: 51,
        where: req.filters,
        attributes: ['id', 'status', 'coin_id', 'prize']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};

exports.post = (req, res) => {

	//var statuses = [1, 2];

	//var resultStatus = statuses[Math.floor(Math.random() * statuses.length)];

	var rand = Math.random() * (10 - 1) + 1;

	var resultStatus = 2;
	if(rand <= 7) {
		resultStatus = 1;
	}

	Users.findOne({
        where: { 'id': req.userId },
        attributes: ['ticket', 'blocked']
    }).then(user => {
		if(user.ticket > 0 && user.blocked == 0) {
			Users.decrement(
				{ ticket: 1 }, 
				{ where: { id: req.userId } 
			}).then(result => {
				if(resultStatus == 1) {
					Coins.findAll({
						order: [['id', 'ASC']],
					}).then(allcoins => {
						var randomCoins = Math.floor(Math.random() * (allcoins.length -1));
						Usercoins.findOne({
							where: { user_id: req.userId, coin_id: allcoins[randomCoins].id },
							attributes: ['total']
						}).then(usercoin => {
							var totalUserCoin = 0;
							if(usercoin != null) {
								totalUserCoin = usercoin.total;
							}
							var amount = 0;
							var randAmount = 500;
	
							if(totalUserCoin * allcoins[randomCoins].price < (allcoins[randomCoins].min_withdraw / 5 * 1)) {
								randAmount = (Math.random() * (500 - 20) + 20).toFixed(4);
								amount = allcoins[randomCoins].min_withdraw / randAmount;
							} else if(totalUserCoin * allcoins[randomCoins].price < (allcoins[randomCoins].min_withdraw / 5 * 2)) {
								randAmount = (Math.random() * (500 - 25) + 25).toFixed(4);
								amount = allcoins[randomCoins].min_withdraw / randAmount;
							} else if(totalUserCoin * allcoins[randomCoins].price < (allcoins[randomCoins].min_withdraw / 5 * 3)) {
								randAmount = (Math.random() * (500 - 33.33) + 33.33).toFixed(4);
								amount = allcoins[randomCoins].min_withdraw / randAmount;
							} else if(totalUserCoin * allcoins[randomCoins].price < (allcoins[randomCoins].min_withdraw / 5 * 4)) {
								randAmount = (Math.random() * (500 - 50) + 50).toFixed(4);
								amount = allcoins[randomCoins].min_withdraw / randAmount;
							} else if(totalUserCoin * allcoins[randomCoins].price < (allcoins[randomCoins].min_withdraw)) {
								randAmount = (Math.random() * (500 - 100) + 100).toFixed(4);
								amount = allcoins[randomCoins].min_withdraw / randAmount;
							} else {
								amount = allcoins[randomCoins].min_withdraw / randAmount;
							}
							Boxes.update(
								{ status: resultStatus, coin_id: allcoins[randomCoins].id, prize: amount, created_at: moment().toDate(), user_id: req.userId },
								{ where: { id: req.body.boxId } }
							).then(result => {
								Stats.increment(
									{ total_laps: 1, total_coins:  allcoins[randomCoins].price * amount }, 
									{ where: { id: 1 } 
								}).then(result => {
									if(usercoin == null) {
										Boxes.create({
											status: 0,
											prize: 0
										}).then(response => {
											Usercoins.create({
												user_id: req.userId,
												coin_id: allcoins[randomCoins].id,
												total: 0
											}).then(response => {
												Usercoins.increment({
													total: amount
												}, { where: { user_id: req.userId, coin_id: allcoins[randomCoins].id }}).then(response => {
													res.status(200).json({
														"status": 200,
														"resultStatus": resultStatus,
														"amount": amount,
														"symbol": allcoins[randomCoins].symbol,
														"coin": allcoins[randomCoins].name,
													});
												}).catch(err => {
													res.status(500).send("Fail! Error -> " + err);
												})
											}).catch(err => {
												res.status(500).send("Fail! Error -> " + err);
											})
										}).catch(err => {
											res.status(500).send("Fail! Error -> " + err);
										})
									} else {
										Boxes.create({
											status: 0,
											prize: 0
										}).then(response => {
											Usercoins.increment({
												total: amount
											}, { where: { user_id: req.userId, coin_id: allcoins[randomCoins].id }}).then(response => {
												res.status(200).json({
													"status": 200,
													"resultStatus": resultStatus,
													"amount": amount,
													"symbol": allcoins[randomCoins].symbol,
													"coin": allcoins[randomCoins].name,
												});
											}).catch(err => {
												res.status(500).send("Fail! Error -> " + err);
											})
										}).catch(err => {
											res.status(500).send("Fail! Error -> " + err);
										})
									}
								}).catch(err =>
									res.status(500).json({
										"error": err
									})
								);
							}).catch(err => {
								res.status(500).json({
									"status": 500,
									"error": err
								});
							});
						}).catch(err => {
							res.status(500).json({
								"error": err
							});
						});
					}).catch(err => {
						res.status(500).json({
							"error": err
						});
					});
				} else {
					Boxes.update(
						{ status: resultStatus, created_at: moment().toDate(), user_id: req.userId },
						{ where: { id: req.body.boxId } }
					).then(result => {
						Stats.increment(
							{ total_laps: 1 }, 
							{ where: { id: 1 } 
						}).then(result =>
							Boxes.create({
								status: 0,
								prize: 0
							}).then(response => {
								res.status(200).json({
									"status": 200,
									"resultStatus": resultStatus
								})
							}).catch(err => {
								res.status(500).send("Fail! Error -> " + err);
							})
						).catch(err =>
							res.status(500).json({
								"error": err
							})
						);
					}).catch(err => {
						res.status(500).json({
							"status": 500,
							"error": err
						});
					});
				}
			}).catch(err =>
				res.status(500).json({
					"error": err
				})
			);
		} else if(user.ticket == 0 && user.blocked == 0) {
			res.status(200).json({
				"status": 200,
				"message": 'NO_TICKET'
			});
		} else {
			res.status(200).json({
				"status": 200,
				"message": 'BLOCKED_USER'
			});
		}
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
	
};