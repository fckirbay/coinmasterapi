const db = require('../config/db.config.js');
const config = require('../config/config.js');
const ScratchWinController = db.scratchwin;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	req.filters['user_id'] = req.userId;
	
	ScratchWinController.findAll({
		where: req.filters,
		order: [['id', 'DESC']],
		attributes: ['user_id', 'token', 'is_completed', 'prize', 'click_date', 'complete_date']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}