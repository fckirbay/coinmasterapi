const config = require('../config/config.js');
const db = require('../config/db.config.js');
var moment = require('moment');
const Op = db.Sequelize.Op;
const Notifications = db.notifications;
const OneSignal = require('onesignal-node');    

exports.sendNotifications = (req, res) => {

	const client = new OneSignal.Client(config.oneSignalAppId, config.oneSignalApiKey);
	var count = 0;

	Notifications.findAll({
        where: {
			time: {
            	[Op.lte]: moment().toDate()
        	},
			status: 0
		},
		order: [['id', 'ASC']],
	}).then(notifies => {

		console.log("notifies", notifies);

		notifies.forEach((notify) => {
			if(notify.firebase.length >= 36) {
				var notification = {
					contents: {
					  'tr': notify.notification,
					  'en': notify.notification,
					},
					headings: {
						'tr': notify.title,
						'en': notify.title,
					},
					small_icon: 'ic_stat_onesignal_default',
					include_player_ids: [notify.firebase.toString()]
				};
	
				client.createNotification(notification).then(response => {
					count++;
					if(count == notifies.length) {
						Notifications.update(
							{ status: 1 },
							{ where: 
								{ 
									time: { [Op.lte]: moment().toDate() },
									status: 0
								} 
						}).then(result =>
								console.log("Bildirimler gönderildi: "+ moment().toDate())
						).catch(err =>
							res.status(200).send({ errorCode: 110 })
						);
					}
				}).catch(e => {
					console.log("bildirim gönderme hatası", e);
				});
			}
            
        });
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};