const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Offerwall = db.offerwall;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	req.filters['user_id'] = req.userId;
	
	Offerwall.findAll({
		where: req.filters,
		attributes: ['user_id', 'tx_id', 'point_value', 'usd_value', 'offer_title', 'is_completed', 'media', 'complete_date']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}