const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Coins = db.coins;
const Scratches = db.scratches;
const Usercoins = db.usercoins;
const Notifications = db.notifications;
const Users = db.users;
const Op = db.Sequelize.Op;
var moment = require('moment');
const coinsModel = require('../model/coins.model.js');

exports.get = (req, res) => {

    req.filters['user_id'] = req.userId;

    var selectedCoins = [];
    var counts = {};
    var coins = {};
    var coinList = [];
    var winnerCoin = null;
    var winnerCoinId = 0;

    Users.findOne({
        where: { id: req.userId },
        attributes: ['firebase']
    }).then(user => {
        Scratches.findOne({
            where: {
                'user_id': req.userId,
                'created_at': {
                    [Op.gte]: moment().subtract(1, 'days').toDate()
                }
            },
            attributes: ['id', 'user_id', 'box_1', 'box_2', 'box_3', 'box_4', 'box_5', 'box_6', 'box_7', 'box_8', 'box_9', 'win', 'status', 'amount', 'created_at']
        }).then(data => {
            if(data == null) {
                Coins.findAll({
                    order: [['id', 'ASC']],
                }).then(allcoins => {
                    allcoins.forEach((coin) => {
                        coins[coin.id] = coin.symbol.toLowerCase();
                        coinList.push(coin.symbol.toLowerCase());
                        counts[coin.symbol.toLowerCase()] = 0;
                    });
                    for (var i = 0; i < 9; i++) {
                        var rand = Math.floor(Math.random() * coinList.length) + 1;
                        var isWinner = 0;
                        coinList.forEach(coin => {
                            if(counts[coin] == 3) {
                                isWinner++;
                            }
                        });
                        if(isWinner > 3) {
                            i--;
                        } else {
                            counts[coins[rand]] = counts[coins[rand]] + 1;
                            if(counts[coins[rand]] == 3) {
                                winnerCoin = coins[rand];
                                winnerCoinId = rand;
                            }
                            selectedCoins.push(coins[rand]);
                        }
                    }
                    var createdAt = moment().toDate();
                    if(winnerCoin == null) {
                        Scratches.create({
                            user_id: req.userId,
                            box_1: selectedCoins[0],
                            box_2: selectedCoins[1],
                            box_3: selectedCoins[2],
                            box_4: selectedCoins[3],
                            box_5: selectedCoins[4],
                            box_6: selectedCoins[5],
                            box_7: selectedCoins[6],
                            box_8: selectedCoins[7],
                            box_9: selectedCoins[8],
                            win: winnerCoin,
                            status: 0,
                            amount: 0,
                            created_at: createdAt
                        }).then(scratch => {
                            Notifications.create({
                                user_id: req.userId,
                                title: 'Your scratchcard is ready!',
                                notification: 'You can scratch your new free scratchcard right now and win the grand prize!',
                                firebase: user.firebase,
                                time: moment().add(1, 'days').utc().format("YYYY-MM-DD HH:mm:ss"),
                                status: 0,
                                created_at: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
                            }).then(notify => {
                                res.status(200).json({
                                    "data": selectedCoins,
                                    "winner": winnerCoin,
                                    "status": '0',
                                    "amount": 0,
                                    "id": scratch.id,
                                    "createdAt": moment(createdAt).format('YYYY-MM-DD HH:mm:ss')
                                });
                            }).catch(err => {
                                res.status(500).send("Fail! Error -> " + err);
                            });
                        }).catch(err => {
                            res.status(500).send("Fail! Error -> " + err);
                        });
                    } else {
                        allcoins.forEach((coin) => {
                            if(coin.id == winnerCoinId) {
                                Usercoins.findOne({
                                    where: { 'user_id': req.userId, coin_id: winnerCoinId },
                                    attributes: ['total']
                                }).then(usercoin => {
                                    var totalUserCoin = 0;
                                    if(usercoin != null) {
                                        totalUserCoin = usercoin.total;
                                    }
                                    var amount = 0;
                                    var randAmount = 500;
    
                                    if(totalUserCoin * coin.price < (coin.min_withdraw / 5 * 1)) {
                                        randAmount = (Math.random() * (500 - 20) + 20).toFixed(4);
                                        amount = coin.min_withdraw / randAmount;
                                    } else if(totalUserCoin * coin.price < (coin.min_withdraw / 5 * 2)) {
                                        randAmount = (Math.random() * (500 - 25) + 25).toFixed(4);
                                        amount = coin.min_withdraw / randAmount;
                                    } else if(totalUserCoin * coin.price < (coin.min_withdraw / 5 * 3)) {
                                        randAmount = (Math.random() * (500 - 33.33) + 33.33).toFixed(4);
                                        amount = coin.min_withdraw / randAmount;
                                    } else if(totalUserCoin * coin.price < (coin.min_withdraw / 5 * 4)) {
                                        randAmount = (Math.random() * (500 - 50) + 50).toFixed(4);
                                        amount = coin.min_withdraw / randAmount;
                                    } else if(totalUserCoin * coin.price < (coin.min_withdraw)) {
                                        randAmount = (Math.random() * (500 - 100) + 100).toFixed(4);
                                        amount = coin.min_withdraw / randAmount;
                                    } else {
                                        amount = coin.min_withdraw / randAmount;
                                    }
                                    Scratches.create({
                                        user_id: req.userId,
                                        box_1: selectedCoins[0],
                                        box_2: selectedCoins[1],
                                        box_3: selectedCoins[2],
                                        box_4: selectedCoins[3],
                                        box_5: selectedCoins[4],
                                        box_6: selectedCoins[5],
                                        box_7: selectedCoins[6],
                                        box_8: selectedCoins[7],
                                        box_9: selectedCoins[8],
                                        win: winnerCoin,
                                        status: 0,
                                        amount: amount,
                                        created_at: moment().toDate()
                                    }).then(scratch => {
                                        Notifications.create({
                                            user_id: req.userId,
                                            title: 'Your scratchcard is ready!',
                                            notification: 'You can scratch your new free scratchcard right now and win the grand prize!',
                                            firebase: user.firebase,
                                            time: moment().add(1, 'days').utc().format("YYYY-MM-DD HH:mm:ss"),
                                            status: 0,
                                            created_at: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
                                        }).then(notify => {
                                            res.status(200).json({
                                                "data": selectedCoins,
                                                "winner": winnerCoin,
                                                "status": '0',
                                                "amount": amount,
                                                "id": scratch.id,
                                                "createdAt": moment(createdAt).format('YYYY-MM-DD HH:mm:ss')
                                            });
                                        }).catch(err => {
                                            res.status(500).send("Fail! Error -> " + err);
                                        });
                                    }).catch(err => {
                                        res.status(500).send("Fail! Error -> " + err);
                                    });
                                }).catch(err => {
                                    res.status(500).json({
                                        "error": err
                                    });
                                });
                            }
                        });
                    }
                    
                    
                }).catch(err => {
                    res.status(500).json({
                        "error": err
                    });
                });
            } else {
                res.status(200).json({
                    "data": [data.box_1, data.box_2, data.box_3, data.box_4, data.box_5, data.box_6, data.box_7, data.box_8, data.box_9],
                    "winner": data.win,
                    "amount": data.amount,
                    "status": data.status,
                    "id": data.id,
                    "createdAt": moment(data.created_at).format('YYYY-MM-DD HH:mm:ss')
                });
            }
        }).catch(err => {
            res.status(500).json({
                "error": err
            });
        });
    }).catch(err => {
        res.status(500).json({
            "error": err
        });
    });

    
};

