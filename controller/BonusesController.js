const db = require('../config/db.config.js');
const Bonuses = db.bonuses;

exports.get = (req, res) => {
	Bonuses.findAll({
		order: [['id', 'ASC']],
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};