const db = require('../config/db.config.js');
var moment = require('moment');
const References = db.references;

exports.get = (req, res) => {

	req.filters['reference_id'] = req.userId;

	References.findAll({
		where: req.filters,
		order: [['id', 'DESC']],
		attributes: ['username', 'first_name', 'last_name', 'country', 'createdAt']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}