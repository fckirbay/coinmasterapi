const db = require('../config/db.config.js');
const config = require('../config/config.js');
var CountryCodes = require("../node_modules/countrycodes/countryCodes.js");
var geoip = require('geoip-country');
const uuidv4 = require('uuid/v4');
var moment = require('moment');

const User = db.user;
const Notifications = db.notifications;
const Role = db.role;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
const request = require('request');

exports.signup = (req, res) => {
	// Save User to Database
	//console.log("Processing func -> SignUp");

	var refresh_token = uuidv4();

	var userIp = req.connection.remoteAddress;
	var country = geoip.lookup(userIp).country;

	if(req.body.reference_id != null && req.body.reference_id != "") {
		User.findOne({
			where: {id: req.body.reference_id},
			attributes: ['verification']
		}).then(verifiedUser => {
			if(verifiedUser == null) {
				res.status(200).send({ errorCode: 120 });
			} else {
				var refId = req.body.reference_id;
				var ticketCount = 10;
				User.create({
					name: req.body.username,
					username: req.body.username,
					email: req.body.username+'@email.com',
					password: bcrypt.hashSync(req.body.password, 8),
					ticket: ticketCount,
					reference_id: refId,
					verification: 0,
					lang: 'en',
					refresh_token: refresh_token,
					refresh_token_expire: moment().add(365, 'days').format("YYYY-MM-DD HH:mm:ss"),
					reference: refId,
					country: country,
					ip_address: userIp
				}).then(user => {
					var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
					if (!passwordIsValid) {
						return res.status(200).send({ auth: false, accessToken: null, reason: "invalid_password" });
					}
					var token = jwt.sign({ id: user.id, isVerify: 0, lang: 'en'}, config.secret, {
						expiresIn: 60 * 60 * 24 * 365 // expires in 1 year
					});

					User.increment(
						{ ticket: 1, reference_count: 1 }, 
						{ where: { id: refId } 
					}).then(result =>
						Notifications.create({
							user_id: 1,
							title: 'Yeni üye kaydı!',
							notification: country +' ülkesinden ' + req.body.username + ' adında biri üye oldu!' ,
							firebase: '8526a0c6-f3da-4973-aac7-726a8c240475',
							time: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
							status: 0,
							created_at: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
						}).then(notify => {
							res.status(200).send({ auth: true, accessToken: token, refreshToken: refresh_token });
						}).catch(err => {
							res.status(500).send("Fail! Error -> " + err);
						})
					).catch(err =>
						res.status(500).json({
							"error": err
						})
					);
				}).catch(err => {
					res.status(500).send("Fail! Error -> " + err);
				});
			}
		}).catch(err => {
			res.status(200).send({ errorCode: 120 });
		});
	} else {
		User.create({
			name: req.body.username,
			username: req.body.username,
			email: req.body.username+'@email.com',
			password: bcrypt.hashSync(req.body.password, 8),
			ticket: 5,
			reference_id: null,
			verification: 0,
			lang: 'en',
			refresh_token: refresh_token,
			refresh_token_expire: moment().add(365, 'days').format("YYYY-MM-DD HH:mm:ss"),
			country: country,
			ip_address: userIp
		}).then(user => {
			var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
			if (!passwordIsValid) {
				return res.status(200).send({ auth: false, accessToken: null, reason: "invalid_password" });
			}
			var token = jwt.sign({ id: user.id, isVerify: 0, lang: 'en' }, config.secret, {
			  expiresIn: 60 * 60 * 24 * 365 // expires in 24 hours
			});

			Notifications.create({
				user_id: 1,
				title: 'Yeni üye kaydı!',
				notification: country +' ülkesinden ' + req.body.username + ' adında biri üye oldu!' ,
				firebase: '8526a0c6-f3da-4973-aac7-726a8c240475',
				time: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
				status: 0,
				created_at: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
			}).then(notify => {
				res.status(200).send({ auth: true, accessToken: token, refreshToken: refresh_token });
			}).catch(err => {
				res.status(500).send("Fail! Error -> " + err);
			});
		}).catch(err => {
			console.log("err", err);
			res.status(500).send("Fail! Error -> " + err);
		});
	}
	
};

exports.verify = (req, res) => {


	User.findOne({
		where: {phone: req.body.country + req.body.phone, verification: 1},
		attributes: ['verification']
	}).then(isUsed => {
		if(isUsed != null) {
			res.status(200).send({ errorCode: 111 })
		} else {
			User.findOne({
				where: {id: req.userId},
				attributes: ['verification_tries']
			}).then(user => {
				if(user.dataValues.verification_tries < 5) {
					var options = {
						uri: 'https://api.checkmobi.com/v1/validation/request',
						method: 'POST',
						json: {
							'number': req.body.country + req.body.phone,
							'type': 'reverse_cli',
							'platform': 'web'
						},
						headers: { 
							'Accept': 'application/json',
							'Content-Type' : 'application/json',
							'Authorization' : '118BCE13-26E1-4E1B-BC68-8567F226F9F7'
						}
					};

					request(options, function (error, response, body) {
						if(body.error) {
							res.status(200).send({ errorCode: 108 });
						} else {
							User.update(
								{ phone: req.body.country + req.body.phone, verification_tries: user.dataValues.verification_tries + 1, country_code: req.body.country, country: CountryCodes.getISO2(req.body.country) },
								{ where: { id: req.userId } }
								).then(result =>
								res.status(200).json({
									"status": 200,
									"id": body.id
								})
								).catch(err =>
								res.status(200).send({ errorCode: 110 })
								)
							}
						})
				} else {
					res.status(200).send({ errorCode: 107 });
				}
			}).catch(err => {
				res.status(500).json({
					"error": err
				});
			})
		}
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.verifyComplete = (req, res) => {

	var options = {
		uri: 'https://api.checkmobi.com/v1/validation/verify',
		method: 'POST',
		json: {
			'id': req.body.id,
			'pin': req.body.pin
		},
		headers: { 
			'Accept': 'application/json',
			'Content-Type' : 'application/json',
			'Authorization' : '118BCE13-26E1-4E1B-BC68-8567F226F9F7'
		}
	};


	request(options, function (error, response, body) {
		if(body.error) {
			res.status(200).send({ errorCode: 109 });
		} else {
			if(body.validated === true) {

				var token = jwt.sign({ id: req.userId, isVerify: 1, lang: 'en' }, config.secret, {
					expiresIn: 60 * 60 * 24 * 365 // expires in 24 hours
				});

				User.update(
					{ verification: 1 },
					{ where: { id: req.userId } }
				)
				.then(result =>

					User.findOne({
						where: req.userId,
						attributes: ['reference_id']
					}).then(user => {
						User.increment(
							{ ticket: 1, reference_count: 1 }, 
							{ where: { id: user.reference_id } 
						}).then(result =>
						    res.status(200).json({
								"status": 200,
								"token": token
							})
						).catch(err =>
						    res.status(500).json({
								"error": err
							})
						);
					}).catch(err => {
						res.status(500).json({
							"error": err
						});
					})
					
				)
				.catch(err =>
					res.status(200).send({ errorCode: 110 })
					)
			} else {
				res.status(200).send({ errorCode: 110 });
			}
		}
	})
	
}

exports.signin = (req, res) => {
	//console.log("Sign-In");
	
	User.findOne({
		where: {
			username: req.body.username
		}
	}).then(user => {
		if (!user) {
			return res.status(200).send({ auth: false, accessToken: null, refreshToken: null, reason: "user_not_found" });
		}

		var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
		if (!passwordIsValid) {
			return res.status(200).send({ auth: false, accessToken: null, refreshToken: null, reason: "invalid_password" });
		}

		if(user.blocked == 1) {
			return res.status(200).send({ auth: false, accessToken: null, refreshToken: null, reason: "blocked_user" });
		}
		
		var token = jwt.sign({ id: user.id, isVerify: user.verification, lang: user.lang }, config.secret, {
		  expiresIn: 60 * 60 * 24 * 365 // expires in 24 hours
		});
		
		res.status(200).send({ auth: true, accessToken: token, refreshToken: user.refresh_token });
		
	}).catch(err => {
		res.send(err);
	});
}

exports.userContent = (req, res) => {

	req.userId = 1;
	
	User.findOne({
		where: {id: req.userId},
		attributes: ['name', 'username', 'email'],
		include: [{
			model: Role,
			attributes: ['id', 'name'],
			through: {
				attributes: ['userId', 'roleId'],
			}
		}]
	}).then(user => {
		res.status(200).json({
			"description": "User Content Page",
			"user": user
		});
	}).catch(err => {
		res.status(500).json({
			"description": "Can not access User Page",
			"error": err
		});
	})
}

exports.adminBoard = (req, res) => {
	User.findOne({
		where: {id: req.userId},
		attributes: ['name', 'username', 'email'],
		include: [{
			model: Role,
			attributes: ['id', 'name'],
			through: {
				attributes: ['userId', 'roleId'],
			}
		}]
	}).then(user => {
		res.status(200).json({
			"description": "Admin Board",
			"user": user
		});
	}).catch(err => {
		res.status(500).json({
			"description": "Can not access Admin Board",
			"error": err
		});
	})
}

exports.managementBoard = (req, res) => {
	User.findOne({
		where: {id: req.userId},
		attributes: ['name', 'username', 'email'],
		include: [{
			model: Role,
			attributes: ['id', 'name'],
			through: {
				attributes: ['userId', 'roleId'],
			}
		}]
	}).then(user => {
		res.status(200).json({
			"description": "Management Board",
			"user": user
		});
	}).catch(err => {
		res.status(500).json({
			"description": "Can not access Management Board",
			"error": err
		});
	})
}