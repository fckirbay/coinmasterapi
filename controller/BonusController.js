const db = require('../config/db.config.js');
var moment = require('moment');
const Op = db.Sequelize.Op;
const Bonus = db.bonus;
const Bonuses = db.bonuses;

exports.get = (req, res) => {
	Bonus.findAll({
		order: [['id', 'ASC']],
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};

exports.post = (req, res) => {

    Bonus.findOne({
        where: {
            [Op.or]: [{user_id: req.userId}, {user_id: 0}],
            code: req.body.bonusCode,
            validity_at: {
                [Op.gte]: moment().toDate()
            }
        }
    }).then(bonusInfo => {
		if(bonusInfo == null) {
			res.status(200).json({
				"status": 200,
				"message": 'INVALID_BONUS_CODE'
			});
		} else {
			Bonuses.findOne({
				where: {
					user_id: req.userId,
					reason: req.body.bonusCode
				}
			}).then(usedBonus => {
				if(usedBonus == null) {
					Bonuses.create({
						user_id: req.userId,
						bonus_amount: bonusInfo.amount,
						reason: req.body.bonusCode,
						date: moment().utc().format("YYYY-MM-DD HH:mm:ss")
					}).then(saveResult => {
						res.status(200).json({
							"status": 200,
							"message": 'OK',
							"amount": bonusInfo.amount
						});
					}).catch(err => {
						res.status(500).send("Fail! Error -> " + err);
					})
				} else {
					res.status(200).json({
						"status": 200,
						"message": 'ALREADY_USED_BONUS_CODE'
					});
				}
			}).catch(err => {
				res.status(500).json({
					"error": err
				});
			});
		}
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
    
};