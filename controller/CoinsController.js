const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Coins = db.coins;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {
	Coins.findAll({
		order: [['id', 'ASC']],
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}