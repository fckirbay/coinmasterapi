const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Support = db.support;
const Op = db.Sequelize.Op;
var moment = require('moment');
const currentDate = new Date();

exports.get = (req, res) => {
	Support.findAll({
		where:  {
			user_id: req.userId,
			[Op.or]: [
		        {
		          'validity_date': { [Op.lte]: currentDate },
		        },
		        {
		          'validity_date': null,
		        },
		    ]
		},
		order: [['id', 'ASC']],
		attributes: ['user_id', 'message', 'owner', 'date', 'validity_date', 'is_viewed'],
		limit: 30
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};

exports.post = (req, res) => {

	var owner = 1;
	if(req.userId == 1) {
		owner = 2;
	}

    Support.create({
		user_id: req.userId,
		message: req.body.message,
		owner: owner,
		date: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
		is_viewed: 0
	}).then(game => {
		res.status(200).json({
			"status": 200,
			"message": "OK"
		});
	}).catch(err => {
		res.status(500).send("Fail! Error -> " + err);
	})
    
};