module.exports = (sequelize, Sequelize) => {
	const Coins = sequelize.define('coins', {
	  id: {
		  type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
	  },
	  name: {
		  type: Sequelize.STRING
	  },
	  symbol: {
		  type: Sequelize.STRING
	  },
	  slug: {
		  type: Sequelize.STRING
	  },
	  cmc_rank: {
		  type: Sequelize.STRING
	  },
	  circulating_supply: {
		  type: Sequelize.STRING
	  },
	  total_supply: {
		  type: Sequelize.STRING
	  },
	  max_supply: {
		  type: Sequelize.STRING
	  },
	  price: {
		  type: Sequelize.STRING
	  },
	  min_withdraw: {
		type: Sequelize.STRING
	  },
	  volume_24h: {
		  type: Sequelize.STRING
	  },
	  percent_change_1h: {
		  type: Sequelize.STRING
	  },
	  percent_change_24h: {
		  type: Sequelize.STRING
	  },
	  percent_change_7d: {
		  type: Sequelize.STRING
	  },
	  market_cap: {
		  type: Sequelize.STRING
	  },
	  last_update: {
		  type: Sequelize.DATE
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Coins;
}