module.exports = (sequelize, Sequelize) => {
	const Stats = sequelize.define('stats', {
	  total_games: {
		  type: Sequelize.STRING
	  },
	  total_laps: {
		  type: Sequelize.STRING
	  },
	  total_coind: {
		  type: Sequelize.STRING
	  }
	}, {
	    freezeTableName: true
	}, {
	    timestamps: false
	});
	
	return Stats;
}