module.exports = (sequelize, Sequelize) => {
	const Scratches = sequelize.define('scratches', {
	  user_id: {
		  type: Sequelize.STRING
	  },
	  box_1: {
		  type: Sequelize.STRING
	  },
	  box_2: {
		  type: Sequelize.STRING
	  },
	  box_3: {
		  type: Sequelize.STRING
	  },
	  box_4: {
		  type: Sequelize.STRING
	  },
      box_5: {
		  type: Sequelize.STRING
	  },
      box_6: {
        type: Sequelize.STRING
      },
      box_7: {
        type: Sequelize.STRING
      },
      box_8: {
        type: Sequelize.STRING
      },
      box_9: {
        type: Sequelize.STRING
      },
	  win: {
		  type: Sequelize.STRING
	  },
	  status: {
		  type: Sequelize.STRING
	  },
	  amount: {
		  type: Sequelize.STRING
	  },
	  created_at: {
		  type: Sequelize.DATE
	  }
	}, {
	    freezeTableName: true,
		timestamps: false
	});
	
	return Scratches;
}