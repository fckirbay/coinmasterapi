module.exports = (sequelize, Sequelize) => {
	const Withdraws = sequelize.define('withdraws', {
	  id: {
		  type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
	  },
	  user_id: {
		  type: Sequelize.INTEGER
	  },
	  coin_id: {
		  type: Sequelize.INTEGER
	  },
	  amount: {
		  type: Sequelize.STRING
	  },
      status: {
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE
      },
      approved_at: {
        type: Sequelize.DATE
      }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Withdraws;
}