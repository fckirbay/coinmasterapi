module.exports = (sequelize, Sequelize) => {
	const Userjokers = sequelize.define('user_jokers', {
	  user_id: {
		  type: Sequelize.STRING
	  },
	  double_earnings: {
		  type: Sequelize.STRING
	  },
	  pass: {
		  type: Sequelize.STRING
	  },
	  show_result: {
		  type: Sequelize.STRING
	  },
	  continue_left: {
		  type: Sequelize.STRING
	  }
	}, {
    	timestamps: false
	});
	
	return Userjokers;
}