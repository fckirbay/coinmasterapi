module.exports = (sequelize, Sequelize) => {
	const Offers = sequelize.define('offers', {
	  id: {
		  type: Sequelize.STRING,
		  primaryKey: true
	  },
	  offer_id: {
		type: Sequelize.STRING
	  },
	  media: {
		  type: Sequelize.STRING
	  },
	  offer_type: {
		  type: Sequelize.STRING
	  },
	  name: {
		  type: Sequelize.STRING
	  },
	  conversion_instructions_long: {
		  type: Sequelize.STRING
	  },
	  tracking_link: {
		type: Sequelize.STRING
	  },
	  icon: {
		type: Sequelize.STRING
	  },
	  description: {
		type: Sequelize.STRING
	  },
	  conversion_instructions_short: {
		type: Sequelize.STRING
	  },
	  payout: {
		type: Sequelize.INTEGER
	  },
	  user_agent: {
		type: Sequelize.STRING
	  },
	  mobile_only: {
		type: Sequelize.INTEGER
	  },
	  categories: {
		type: Sequelize.STRING
	  },
	  countries: {
		type: Sequelize.STRING
	  },
	  status: {
		type: Sequelize.INTEGER
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Offers;
}