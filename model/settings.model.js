module.exports = (sequelize, Sequelize) => {
	const Settings = sequelize.define('settings', {
	  ads: {
		  type: Sequelize.STRING
	  },
	  offerwall: {
		  type: Sequelize.STRING
	  }
	}, {
	    freezeTableName: true
	}, {
	    timestamps: false
	});
	
	return Settings;
}