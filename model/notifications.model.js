module.exports = (sequelize, Sequelize) => {
	const Notifications = sequelize.define('notifications', {
	  user_id: {
		  type: Sequelize.STRING
	  },
	  title: {
		  type: Sequelize.STRING
	  },
	  notification: {
		  type: Sequelize.STRING
	  },
	  firebase: {
		  type: Sequelize.STRING
	  },
	  time: {
		  type: Sequelize.STRING
	  },
	  status: {
		  type: Sequelize.STRING
	  },
	  created_at: {
		  type: Sequelize.STRING
	  },
	  updated_at: {
		  type: Sequelize.STRING
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Notifications;
}