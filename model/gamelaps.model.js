module.exports = (sequelize, Sequelize) => {
	const Gamelaps = sequelize.define('game_laps', {
	  id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
	  },
	  game_id: {
		  type: Sequelize.STRING
	  },
	  card_1: {
		  type: Sequelize.STRING
	  },
	  card_2: {
		  type: Sequelize.STRING
	  },
	  used_joker: {
		  type: Sequelize.STRING
	  },
	  result: {
		  type: Sequelize.STRING
	  },
	  createdAt: {
		  type: Sequelize.DATE
	  },
	  updatedAt: {
		  type: Sequelize.DATE
	  }
	}, {
	    freezeTableName: true
	});
	
	return Gamelaps;
}