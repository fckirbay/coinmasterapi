module.exports = (sequelize, Sequelize) => {
	const Support = sequelize.define('support', {
	  user_id: {
		  type: Sequelize.STRING
	  },
	  message: {
		  type: Sequelize.STRING
	  },
	  owner: {
		  type: Sequelize.STRING
	  },
	  date: {
		  type: Sequelize.STRING
	  },
	  validity_date: {
		  type: Sequelize.STRING
	  },
	  is_viewed: {
		  type: Sequelize.STRING
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Support;
}