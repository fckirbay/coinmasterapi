module.exports = (sequelize, Sequelize) => {
	const Scratchwin = sequelize.define('scratch_win', {
	  user_id: {
		  type: Sequelize.STRING
	  },
	  token: {
		  type: Sequelize.STRING
	  },
	  is_completed: {
		  type: Sequelize.STRING
	  },
	  prize: {
		  type: Sequelize.STRING
	  },
	  click_date: {
		  type: Sequelize.STRING
	  },
	  complete_date: {
		  type: Sequelize.STRING
	  }
	}, {
	    freezeTableName: true
	});
	
	return Scratchwin;
}