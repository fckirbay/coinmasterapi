module.exports = (sequelize, Sequelize) => {
	const Bonus = sequelize.define('bonus', {
	  id: {
		  type: Sequelize.STRING,
		  primaryKey: true
	  },
	  code: {
		type: Sequelize.STRING
	  },
	  name: {
		  type: Sequelize.STRING
	  },
	  description: {
		  type: Sequelize.STRING
	  },
	  amount: {
		  type: Sequelize.STRING
	  },
	  type: {
		  type: Sequelize.STRING
	  },
	  created_at: {
		  type: Sequelize.DATE
	  },
	  validity_at: {
		  type: Sequelize.DATE
	  },
	  user_id: {
		  type: Sequelize.STRING
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Bonus;
};