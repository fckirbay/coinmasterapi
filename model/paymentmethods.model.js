module.exports = (sequelize, Sequelize) => {
	const Paymentmethods = sequelize.define('payment_methods', {
	  name: {
		  type: Sequelize.STRING
	  },
	  subtitle: {
		  type: Sequelize.STRING
	  },
	  photo: {
		  type: Sequelize.STRING
	  },
	  url: {
		  type: Sequelize.STRING
	  },
	  coin: {
		  type: Sequelize.STRING
	  },
	  status: {
		  type: Sequelize.STRING
	  }
	}, {
	    freezeTableName: true
	});
	
	return Paymentmethods;
}