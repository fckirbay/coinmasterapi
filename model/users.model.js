module.exports = (sequelize, Sequelize) => {
	const Users = sequelize.define('users2', {
	  name: {
		  type: Sequelize.STRING
	  },
	  username: {
		  type: Sequelize.STRING
	  },
	  email: {
		  type: Sequelize.STRING
	  },
	  password: {
		  type: Sequelize.STRING
	  },
	  first_name: {
		  type: Sequelize.STRING
	  },
	  last_name: {
		  type: Sequelize.STRING
	  },
	  createdAt: {
		  type: Sequelize.STRING
	  },
	  updatedAt: {
		  type: Sequelize.DATE
	  },
	  phone: {
		  type: Sequelize.STRING
	  },
	  photo: {
		  type: Sequelize.STRING
	  },
	  reference: {
		  type: Sequelize.STRING
	  },
	  ticket: {
		  type: Sequelize.STRING
	  },
	  binance: {
		  type: Sequelize.STRING
	  },
	  binance_tr: {
		  type: Sequelize.STRING
	  },
	  clicks: {
		  type: Sequelize.STRING
	  },
	  won: {
		  type: Sequelize.STRING
	  },
	  lost: {
		  type: Sequelize.STRING
	  },
	  earnings: {
		  type: Sequelize.STRING
	  },
	  balance: {
		  type: Sequelize.STRING
	  },
	  reference_id: {
		  type: Sequelize.STRING
	  },
	  reference_count: {
		  type: Sequelize.STRING
	  },
	  blocked: {
		  type: Sequelize.STRING
	  },
	  blocked_reason: {
		  type: Sequelize.STRING
	  },
	  lang: {
		  type: Sequelize.STRING
	  },
	  currency: {
		  type: Sequelize.STRING
	  },
	  country: {
		  type: Sequelize.STRING
	  },
	  os: {
		  type: Sequelize.STRING
	  },
	  firebase: {
		  type: Sequelize.STRING
	  },
	  walkthrough: {
		  type: Sequelize.STRING
	  },
	  verification: {
		  type: Sequelize.STRING
	  },
	  verification_tries: {
		  type: Sequelize.STRING
	  },
	  purchasing: {
		  type: Sequelize.STRING
	  },
	  premium_membership: {
		  type: Sequelize.STRING
	  },
	  premium_expiration: {
		  type: Sequelize.STRING
	  },
	  is_notify: {
		  type: Sequelize.STRING
	  },
	  refresh_token: {
		  type: Sequelize.STRING
	  },
	  refresh_token_creation: {
		  type: Sequelize.STRING
	  },
	  refresh_token_expire: {
		  type: Sequelize.STRING
	  },
	  app_version: {
		  type: Sequelize.STRING
	  },
	  ip_address: {
		  type: Sequelize.STRING
	  }
	});
	
	return Users;
}