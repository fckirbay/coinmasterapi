module.exports = (sequelize, Sequelize) => {
	const Games = sequelize.define('games', {
	  id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
	  },
	  user_id: {
		  type: Sequelize.STRING
	  },
	  lap: {
		  type: Sequelize.STRING
	  },
	  coins: {
		  type: Sequelize.STRING
	  },
	  is_completed: {
		  type: Sequelize.STRING
	  },
	  createdAt: {
		  type: Sequelize.DATE
	  },
	  updatedAt: {
		  type: Sequelize.DATE
	  }
	}, {
	    freezeTableName: true
	});
	
	return Games;
}