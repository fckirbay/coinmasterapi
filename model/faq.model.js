module.exports = (sequelize, Sequelize) => {
	const Faq = sequelize.define('faq', {
	  id: {
		  type: Sequelize.STRING,
		  primaryKey: true
	  },
	  question: {
		  type: Sequelize.STRING
	  },
	  answer: {
		  type: Sequelize.STRING
	  },
	  lang: {
		type: Sequelize.STRING
	  },
	  orders: {
		  type: Sequelize.STRING
	  }
	}, {
		timestamps: false,
	    freezeTableName: true
	});
	
	return Faq;
};