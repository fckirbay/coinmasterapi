module.exports = (sequelize, Sequelize) => {
	const Clickrewarded = sequelize.define('click_rewarded', {
	  id: {
		  type: Sequelize.STRING,
		  primaryKey: true
	  },
	  user_id: {
		  type: Sequelize.STRING
	  },
	  token: {
		  type: Sequelize.STRING
	  },
	  type: {
		  type: Sequelize.STRING
	  },
	  is_completed: {
		  type: Sequelize.STRING
	  },
	  click_date: {
		  type: Sequelize.STRING
	  },
	  complete_date: {
		  type: Sequelize.STRING
	  }
	}, {
		timestamps: false,
	    freezeTableName: true
	});
	
	return Clickrewarded;
}