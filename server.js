var express = require('express');
var app = express();
var cors = require('cors')
var bodyParser = require('body-parser');
var createError = require('http-errors');
var path = require('path');
var i18n = require('i18n');
var cookieParser = require('cookie-parser');
var cron = require('node-cron');

app.use(bodyParser.json());

const db = require('./config/db.config.js');

const Role = db.role;
  
// force: true will drop the table if it already exists
/*db.sequelize.sync({force: true}).then(() => {
  console.log('Drop and Resync with { force: true }');
  initial();
});*/
 
//require('./app/route/project.route.js')(app);

i18n.configure({
  // setup some locales - other locales default to en silently
  locales: ['en', 'tr', 'de'],
  // sets a custom cookie name to parse locale settings from
  cookie: 'lang',
  // where to store json files - defaults to './locales'
  directory: __dirname + '/locales'
});


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors())

app.use(i18n.init);

require('./router/api.js')(app);

coincap = require('./controller/CoincapController');
crons = require('./controller/CronsController');


// catch 404 and forward to error handler
/*
app.use(function(req, res, next) {
  next(createError(404));
});
*/
// error handler
/*
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
*/

// Create a Server
var server = app.listen(db.port, function () {
 
  var host = server.address().address
  var port = server.address().port
 
  console.log("App listening at http://%s:%s", host, port)
});

/*
cron.schedule('* * * * *', () => {
  coincap.get();
});
*/

cron.schedule('* * * * *', () => {
  crons.sendNotifications();
});

function initial(){
	Role.create({
		id: 1,
		name: "USER"
	});
	
	Role.create({
		id: 2,
		name: "ADMIN"
	});
	
	Role.create({
		id: 3,
		name: "PM"
	});
}